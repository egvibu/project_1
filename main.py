import requests
import json
from pydantic import BaseModel, ValidationError, Field


class Coord(BaseModel):
    lon: float
    lat: float


class Weather(BaseModel):
    id: int
    main: str
    description: str
    icon: str

class Main(BaseModel):
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    pressure: int
    humidity: int

class Wind(BaseModel):
    speed: int
    deg: int

class Rain(BaseModel):
    h: float = Field(alias='1h')

class Clouds(BaseModel):
    all: int

class Sys(BaseModel):
    type: int
    id: int
    country: str
    sunrise: int
    sunset: int


class CityWeather(BaseModel):
    coord: Coord
    weather: list[Weather]
    base: str
    main: Main
    visibility: int
    wind: Wind
    rain: Rain
    clouds: Clouds
    dt: int
    sys: Sys
    timezone: int
    id: int
    name: str
    cod: int


response = requests.get("https://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=962b165438a79a9361b5e33cf06ef76f")


try:
    cityWeather = CityWeather.parse_raw(json.dumps(response.json()))
except ValidationError as e:
    print("Exception", e.json())
else:
    print(cityWeather)

# print(cityWeather.weather[0].id)

# print(response.json())
